<?php

namespace App\Http\Controllers\API;

use \App\User;
use Laravel\Passport\Passport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function register(Request $request)
    {
        $validate_data = $request->validate([
            'name'=>'required|max:55',
            'email'=>'email|required|unique:users',
            'password'=>'required|confirmed',
        ]);
        $validate_data['password'] = bcrypt($request->password);
        $user = User::create($validate_data);
        $accesstoken = $user->createToken('android')->accessToken;
        return response(['user'=>$user,'access_token'=>$accesstoken]);
    }
    public function login(Request $request)
    {
        $login_data = $request->validate([
            'email'=>'required',
            'password'=>'required'
        ]);
        if(!auth()->attempt($login_data)){
            return response(['message'=>'Invalid login']);
        }
        $accesstoken = auth()->user()->createToken('android')->accessToken;
        return response(['user'=> auth()->user(),'access_token'=>$accesstoken]);

    }
    public function user()
    {
        $user = User::all();
        return response(['user'=>$user]);
    }
}
